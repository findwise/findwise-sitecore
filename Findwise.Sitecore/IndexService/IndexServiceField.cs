﻿namespace Findwise.Sitecore.IndexService
{
    /// <summary>
    /// Represents a field on a document in the i3 Index Service. 
    /// </summary>
    public class IndexServiceField
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public IndexServiceField(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}