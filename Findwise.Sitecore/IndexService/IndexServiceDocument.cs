﻿using System.Collections.Generic;

namespace Findwise.Sitecore.IndexService
{
    /// <summary>
    /// Represents a document in the i3 Index Service. 
    /// </summary>
    public class IndexServiceDocument
    {
        public string Id { get; set; }
        public List<IndexServiceField> Fields { get; set; }
    }
}