﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Xml;
using Findwise.Sitecore.Configuration;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Findwise.Sitecore.IndexService
{
    /// <summary>
    /// Service to send or delete documents in the Findwise i3 Index Service.
    /// </summary>
    public class IndexServiceClient
    {
        public bool IsValidSitecoreItem(Item item)
        {
            //Does the Sitecore Item has a valid template we can use?
            return Settings.SitecoreTemplateIds.Contains(item.TemplateID.ToString());
        }

        public void SendSitecoreItem(Item item)
        {
            var document = MapSitecoreItemToIndexServiceDocument(item);
            SendDocumentToIndexService(document);
        }

        private IndexServiceDocument MapSitecoreItemToIndexServiceDocument(Item item)
        {
            var document = new IndexServiceDocument();
            //Concats item id + item lanuage as document id, so we can support multi languages
            document.Id = item.ID + "/" + item.Language;
            document.Fields = new List<IndexServiceField>();

            foreach (Field field in item.Fields)
            {
                string fieldName = field.Name.ToLower();
                if (Settings.SitecoreToSolrFieldMappings.ContainsKey(fieldName) && !string.IsNullOrEmpty(field.Value))
                {
                    string solrFieldName = Settings.SitecoreToSolrFieldMappings[fieldName].ToLower();
                    var indexServiceField = new IndexServiceField(solrFieldName, field.Value);
                    document.Fields.Add(indexServiceField);
                }
            }
            
            return document;
        }

        
        public void SendDocumentToIndexService(IndexServiceDocument document)
        {
            string documentXml = TransformDocumentToXml(document);

            String url = Settings.IndexServiceUrl + "/rest/" + Settings.IndexServiceCollection + "/documents.xml";
            CallUrlWithData(url, "POST", documentXml);
        }

        public void DeleteSitecoreItem(string sitecoreItemId)
        {
            String url = Settings.IndexServiceUrl + "/rest/" + Settings.IndexServiceCollection + 
                "/documents.xml?id=" + HttpUtility.UrlDecode(sitecoreItemId);
            CallUrl(url, "DELETE");
        }

        /// <summary>
        /// Turns our document object into xml of this format:
        /// <docs>
        ///     <doc id="http://mycms.com/document20.pdf">
        ///         <field name="title" type="string">this is a sample title</field>
        ///         <field name="keywords" type="string">test</field>
        ///         <field name="keywords" type="string">demo</field>
        ///         <field name="body" type="binary">dGVzdA==</field>
        ///     </doc>
        /// </docs>
        /// </summary>
        private string TransformDocumentToXml(IndexServiceDocument document)
        {
            XmlDocument xmlDocument = new XmlDocument();
            var docsElement = xmlDocument.CreateElement("docs");
            xmlDocument.AppendChild(docsElement);

            var docElement = xmlDocument.CreateElement("doc");
            docElement.SetAttribute("id", document.Id);
            docsElement.AppendChild(docElement);

            foreach (var field in document.Fields)
            {
                var fieldElement = xmlDocument.CreateElement("field");
                fieldElement.SetAttribute("name", field.Name);
                //Also supports binary, but we only need string as type
                fieldElement.SetAttribute("type", "string");
                fieldElement.InnerText = field.Value;
                docElement.AppendChild(fieldElement);
            }

            return xmlDocument.OuterXml;
        }

        private void CallUrlWithData(string url, string httpMethod, string xmlData)
        {
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/xml";
                client.UploadString(url, httpMethod, xmlData);
                //TODO: read response
            }
        }

        private void CallUrl(string url, string httpMethod)
        {
            var request = WebRequest.Create(url);
            request.Method = httpMethod;

            using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
            {
                //TODO: read response
            }
        }
    }
}