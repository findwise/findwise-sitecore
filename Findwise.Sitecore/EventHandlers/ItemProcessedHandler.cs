﻿using System;
using System.Diagnostics.CodeAnalysis;
using Common.Logging;
using Findwise.Sitecore.IndexService;
using Sitecore.Publishing.Pipelines.PublishItem;

namespace Findwise.Sitecore.EventHandlers
{
    /// <summary>
    /// Sitecore Processor that gets fired when the item is published.
    /// Feeds item to Findwise i3 Index Service.
    /// </summary>
    public class ItemProcessedHandler
    {
        protected ILog log = LogManager.GetCurrentClassLogger();
        protected IndexServiceClient indexServiceClient = new IndexServiceClient();

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers", Justification = "Sitecore needs to be able to access the method through reflection.")]
        public void HandleItemProcessed(object sender, ItemProcessedEventArgs args)
        {
            //Only handles Publish events
            if (args.Context.Action != global::Sitecore.Publishing.PublishAction.PublishVersion)
            {
                log.Debug(m => m("HandleItemProcessed: action != publish"));
                return;
            }
                
            var masterDatabase = args.Context.PublishOptions.SourceDatabase;
            if (masterDatabase == null)
            {
                log.Debug(m => m("HandleItemProcessed: masterDatabase == null"));
                return;
            }

            var item = masterDatabase.Items.GetItem(args.Context.ItemId, args.Context.PublishOptions.Language);
            if (item == null)
            {
                log.Debug(m => m("HandleItemProcessed: item == null"));
                return;
            }

            if (item.Versions.Count == 0 || 
                item.Publishing.NeverPublish || 
                !item.Publishing.IsPublishable(DateTime.Now, true) ||
                !indexServiceClient.IsValidSitecoreItem(item))
            {
                log.Debug(m => m("HandleItemProcessed: item requirements not met"));
                return;
            }

            try
            {
                indexServiceClient.SendSitecoreItem(item);
            }
            catch(Exception exception)
            {
                log.Error(m => m("HandleItemProcessed: Can't parse or send item to Index Service", exception));
                throw;
            }
        }
    }
}