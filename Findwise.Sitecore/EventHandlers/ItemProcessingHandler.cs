﻿using Common.Logging;
using Findwise.Sitecore.IndexService;
using Sitecore.Publishing.Pipelines.PublishItem;

namespace Findwise.Sitecore.EventHandlers
{
    /// <summary>
    /// Sitecore Processor that gets fired when item is being processed.
    /// Deletes item from Findwise i3 Index Service.
    /// </summary>
    public class ItemProcessingHandler
    {
        protected ILog log = LogManager.GetCurrentClassLogger();
        protected IndexServiceClient indexServiceClient = new IndexServiceClient();
        
        public void HandleItemProcessing(object sender, ItemProcessingEventArgs args)
        {
            //Only handles Delete events
            if (args.Context.Action != global::Sitecore.Publishing.PublishAction.DeleteTargetItem)
            {
                log.Debug(m => m("HandleItemProcessed: action != publish"));
                return;
            }

            var targetDatabase = args.Context.PublishOptions.TargetDatabase;
            if (targetDatabase == null)
            {
                log.Debug(m => m("DeleteTargetItem: targetDatabase == null"));
                return;
            }

            //Deletes this language version of the item (HandleItemProcessing will get fired for each language)
            string documentId = args.Context.ItemId + "/" + args.Context.PublishOptions.Language;
            indexServiceClient.DeleteSitecoreItem(documentId);
        }
    }
}