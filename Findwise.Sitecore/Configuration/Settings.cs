﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Findwise.Sitecore.Configuration
{
    /// <summary>
    /// These are the settings used in the Sitecore event handler / Index Service client. 
    /// TODO: all of them are hardcoded, but should come from either config-file or Sitecore config item.
    /// </summary>
    public static class Settings
    {
        //TODO: replace with id's to Sitecore template guids of the items you want to publish
        public static readonly Collection<string> SitecoreTemplateIds = new Collection<string>
        {
            { "{e9a99def-b79e-41f4-b464-0d6176e6cb69}" },
            { "{76c47a4b-1668-432d-8672-3ac8e3bc7675}" }
        };

        //TODO: replace with list of Sitecore field names to Solr field names (lowercase)
        public static readonly Dictionary<string, string> SitecoreToSolrFieldMappings = new Dictionary<string, string>
        {
            { "name", "title" },
            { "description", "content" }
        };

        //TODO: change to match url and collection of index service
        public static readonly string IndexServiceUrl = "http://localhost:8080";
        public static readonly string IndexServiceCollection = "collection1";

    }
}