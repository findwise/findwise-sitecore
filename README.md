# Sitecore Event Handler to push data into Index Service

## About

This is a simple Sitecore Event Handler. When configurered in your Sitecore installation, all published items will trigger this code. The code extracts the Sitecore items fields, maps the names to Solr field name, and sends the document to the Findwise i3 Index Service.

## Installation

1. Compile and build the C# project

2. Place the Dlls in the Sitecore bin folder (you probably wann exclude the 3rd party dlls)

3. Edit your config file (probably web.config) to use the new event handlers. Find the the section <events> add or update so our event handlers so it looks something like this:

	<event name="publish:itemProcessed">
		<handler type="Findwise.Sitecore.EventHandlers.ItemProcessedHandler, Findwise.Sitecore" method="OnItemProcessed"/>
	</event>

	<event name="publish:itemProcessing">
		<handler type="Findwise.Sitecore.EventHandlers.ItemProcessingHandler, Findwise.Sitecore" method="OnItemProcessing"/>
	</event>