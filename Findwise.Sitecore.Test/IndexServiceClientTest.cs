﻿using System.Collections.Generic;
using Findwise.Sitecore.IndexService;
using NUnit.Framework;

namespace Findwise.Sitecore.Test
{
    [TestFixture]
    public class IndexServiceClientTest
    {
        [Test]
        public void Test_SendDocumentToIndexService()
        {
            var document = new IndexServiceDocument();
            document.Id = "{dc2b4d8c-3f07-49fe-a9fa-81d94bd7c275}/da";
            document.Fields = new List<IndexServiceField>()
            {
                new IndexServiceField("title", "Test 1"),
                new IndexServiceField("content", "Lorum ipsum"),
            };

            var client = new IndexServiceClient();
            client.SendDocumentToIndexService(document);
        }

        [Test]
        public void Test_DeleteSitecoreItem()
        {
            string id = "{dc2b4d8c-3f07-49fe-a9fa-81d94bd7c275}/da";

            var client = new IndexServiceClient();
            client.DeleteSitecoreItem(id);
        }
    }
}